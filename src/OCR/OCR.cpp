#include "stdafx.h"

#include <opencv2/core/utility.hpp>
#include <opencv2/text/ocr.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <baseapi.h>


#include <iostream>
#include <string>

using namespace cv;
using namespace cv::text;
using namespace std;


string bestMatch(Mat im, cv::Rect roi, Ptr<OCRTesseract> ocr);

float MINIMUM_ALLOWED_CONFIDENCE = 0.7;

int DATA_NUMERIC = 0,
	DATA_ALPHA = 1,
	DATA_ALPHA_PUNCT = 2,
	DATA_ALL = 3; // alphanumeric, common punctuation .,!'

tesseract::TessBaseAPI api;

int main()
{
	string imageName("E:\\git\\OCR\\Testing\\4.png"); // by default

	//api.SetVariable("tessedit_char_blacklist", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-'");
	//api.SetVariable("tessedit_char_whitelist", "0123456789\\,.()?");
	api.Init("C:\\Program Files (x86)\\Tesseract-OCR\\", "eng", tesseract::OEM_DEFAULT);
	api.SetPageSegMode(static_cast<tesseract::PageSegMode>(7));
	api.SetOutputName("out");
	api.SetVariable("tessedit_char_whitelist", ":.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-'");
	api.SetVariable("tessedit_char_blacklist", "~0123456789\\()?");


	Ptr<OCRTesseract> ocr1 = OCRTesseract::create(NULL, NULL, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-'", 2);
	//Ptr<OCRTesseract> ocr2 = OCRTesseract::create(NULL, NULL, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 2);
	//Ptr<OCRTesseract> ocr3 = OCRTesseract::create(NULL, NULL, ",.!'-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 2);
	//Ptr<OCRTesseract> ocr4 = OCRTesseract::create(NULL, NULL, ",.!'-+/*0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 2);
	//Ptr<OCRTesseract> ocr_list[4] = { ocr1, ocr2, ocr3, ocr4 };
	vector <Ptr<OCRTesseract>> ocr_list;
	//ocr_list.push_back(ocr1);
	//ocr_list.push_back(ocr2);
	//ocr_list.push_back(ocr3);
	//ocr_list.push_back(ocr4);

	vector<Rect> allROIs;
	vector<int> dataTypes;
	vector<string> categories;

	// 1
	//allROIs.push_back(Rect(663, 338, 512, 80));
	//allROIs.push_back(Rect(845, 172, 300, 17));
	//allROIs.push_back(Rect(845, 217, 300, 19));

	// 2
	//allROIs.push_back(Rect(884, 306, 180, 44));
	//allROIs.push_back(Rect(884, 113, 180, 64));
	//allROIs.push_back(Rect(884, 157, 180, 20));

	//3

	//allROIs.push_back(Rect(813,199,213,38));
	//allROIs.push_back(Rect(813,265,213,38));
	//allROIs.push_back(Rect(1093,197,39,38));
	//allROIs.push_back(Rect(1093,262,39,38));
	//allROIs.push_back(Rect(148,156,346,32));

	// 4

	allROIs.push_back(Rect(719,154,215,33));
	allROIs.push_back(Rect(721,194,487,33));

	// 5

	//allROIs.push_back(Rect(566,155,213,31));
	//allROIs.push_back(Rect(534,205,182,22));
	//allROIs.push_back(Rect(534,231,182,22));
	//allROIs.push_back(Rect(732,205,84,22));
	//allROIs.push_back(Rect(732,231,84,22));

	Mat image;
	image = imread(imageName.c_str(), CV_LOAD_IMAGE_GRAYSCALE); // Read the file

	if (image.empty())                      // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}
	
	//image.convertTo(image, CV_32F);
	//normalize(image, image, 0, 255, NORM_MINMAX);

	for (int i = 0; i < allROIs.size(); i++)
	{
		String output = bestMatch(image, allROIs.at(i), ocr1);
	}
	
	return 0;
}

string bestMatch(Mat im, Rect roi, Ptr<OCRTesseract> ocr)
{
	string best;
	
	//im.convertTo(im, -1, 1.2, 5);


	//Mat temp1(im, roi);
	Mat temp1(im,roi), temp2(im, roi),temp3,temp4,temp5;
	float scale = 90.0 / (float)temp1.rows;
	resize(temp1, temp1, Size(), scale, scale, INTER_CUBIC);
	resize(temp2, temp2, Size(), scale, scale, INTER_CUBIC);

	GaussianBlur(temp2, temp2, Size(3, 3), 0, 0);
	//GaussianBlur(temp1, temp5, Size(3,3), 3);
	//addWeighted(temp1, 1.5, temp5, -0.5, 0, temp1);
	//temp1.convertTo(temp1, -1, 1.5, 50);
	temp1.convertTo(temp1, CV_8U);
	temp2.convertTo(temp2, CV_8U);
	
	//adaptiveThreshold(temp1, temp1, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 13, 3);
	//adaptiveThreshold(temp2, temp2, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 9, 3);
	threshold(temp1, temp1, 65, 255, THRESH_BINARY);
	threshold(temp2, temp2, 65, 255, THRESH_BINARY);
	
	//resize(temp1, temp1, Size(), scale, scale, INTER_NEAREST);
	//resize(temp2, temp2, Size(), scale, scale, INTER_NEAREST);

	int morph_elem = 0;
	int morph_size = 1;

	Mat element = getStructuringElement(MORPH_ELLIPSE, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));
	morphologyEx(temp1, temp3, MORPH_DILATE, element);
	//morphologyEx(temp1, temp1, MORPH_ERODE, element);
	//morphologyEx(temp3, temp3, MORPH_CLOSE, element);
	morphologyEx(temp2, temp4, MORPH_CLOSE, element);

	vector<Mat> images;
	images.push_back(temp1);
	images.push_back(temp2);
	images.push_back(temp3);
	images.push_back(temp4);
	//images.push_back(temp5);

	//morphologyEx(temp2, temp5, MORPH_OPEN, element);
	//morphologyEx(temp5, temp5, MORPH_CLOSE, element);

	namedWindow("1", WINDOW_AUTOSIZE);
	imshow("1", images.at(0));
	namedWindow("2", WINDOW_AUTOSIZE);
	imshow("2", images.at(1));
	namedWindow("3", WINDOW_AUTOSIZE);
	imshow("3", images.at(2));
	namedWindow("4", WINDOW_AUTOSIZE);
	imshow("4", images.at(3));
	//namedWindow("5", WINDOW_AUTOSIZE);
	//imshow("5", images.at(4));

	waitKey(0); // Wait for a keystroke in the window

	//vector<Rect> boxes1, boxes2;
	//vector<string> words1, words2, words;
	vector<float> confidence1, confidence2, confidence3, confidence4, confidence5;
	string out[5] = { "","","", "",""};

	float avgC[5] = { 0,0,0,0,0 };

	for (int i = 0; i < 4; i++)
	{
		Mat temp = images.at(i);
		//ocr->run(images.at(i), out[i], NULL, NULL, &confidence1, OCR_LEVEL_WORD);
		api.SetImage((uchar*)temp.data, temp.size().width, temp.size().height, temp.channels(), temp.step1());
		api.Recognize(0);
		out[i] = api.GetUTF8Text();

		tesseract::ResultIterator* ri = api.GetIterator();
		if (ri != 0)
		{
			do
			{
				const char* word = ri->GetUTF8Text(tesseract::RIL_WORD);
				if (word != 0)
				{
					confidence1.push_back(ri->Confidence(tesseract::RIL_WORD));
				}
				delete[] word;
			} while ((ri->Next(tesseract::RIL_WORD)));

			delete ri;
		}

		for (int j = 0; j < confidence1.size(); j++)
		{
			avgC[i] += confidence1.at(j);
		}
		avgC[i] /= confidence1.size();
		cout << out[i] << "\t" << avgC[i] << endl;
	}


	//ocr->run(temp1, out[0], NULL, NULL, &confidence1, OCR_LEVEL_WORD);
	//ocr->run(temp2, out[1], NULL, NULL, &confidence2, OCR_LEVEL_WORD);
	//ocr->run(temp3, out[2], NULL, NULL, &confidence3, OCR_LEVEL_WORD);
	//ocr->run(temp4, out[3], NULL, NULL, &confidence4, OCR_LEVEL_WORD);
	//ocr->run(temp5, out[4], NULL, NULL, &confidence5, OCR_LEVEL_WORD);

	//float avgC[5] = { 0,0,0,0,0 };
	//
	//
	//for (int i = 0; i < confidence1.size(); i++)
	//{
	//	avgC[0] += confidence1.at(i);
	//}
	//for (int i = 0; i < confidence2.size(); i++)
	//{
	//	avgC[1] += confidence2.at(i);
	//}
	//for (int i = 0; i < confidence3.size(); i++)
	//{
	//	avgC[2] += confidence2.at(i);
	//}
	//for (int i = 0; i < confidence4.size(); i++)
	//{
	//	avgC[3] += confidence4.at(i);
	//}
	//for (int i = 0; i < confidence5.size(); i++)
	//{
	//	avgC[4] += confidence5.at(i);
	//}
	//avgC[0] /= (float)confidence1.size();
	//avgC[1] /= (float)confidence2.size();
	//avgC[2] /= (float)confidence3.size();
	//avgC[3] /= (float)confidence4.size();
	//avgC[4] /= (float)confidence5.size();

	//cout << out[0] << "\t" << avgC[0] << endl;
	//cout << out[1] << "\t" << avgC[1] << endl;
	//cout << out[2] << "\t" << avgC[2] << endl;
	//cout << out[3] << "\t" << avgC[3] << endl;
	//cout << out[4] << "\t" << avgC[4] << endl;

	//float temp = -FLT_MAX;
	//
	//for (int i = 0; i < 3; i++)
	//{
	//	if (avgC[i] > temp)
	//	{
	//		temp = avgC[i];
	//		best = out[i];
	//	}
	//}

	return "";
}