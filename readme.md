Yes, this is really bad code. It's at a very early "let's test the features" sort of stage.

The intent is to generate a rather simple OCR system that will take rectangles (representing text zones) and generate a TSV from this data. Probably with some options for image processing, character sets, and so on.